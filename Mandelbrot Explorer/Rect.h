#pragma once

class Rect
{
public:
	Rect() : top(0), bottom(0), left(0), right(0) {}
	Rect(double t, double b, double l, double r) :
	  top(t), bottom(b), left(l), right(r) {}

	double top;
	double bottom;
	double left;
	double right;
};

inline bool operator==(const Rect& lhs, const Rect& rhs)
{ 
	return ((lhs.top==rhs.top) && (lhs.bottom==rhs.bottom) &&
		   (lhs.left==rhs.left) && (lhs.right==rhs.right));
}

inline bool operator!=(const Rect& lhs, const Rect& rhs) { return !operator==(lhs,rhs); }