#pragma once

#include <boost\thread\mutex.hpp>
#include <string>
#include <SDL.h>
#include <SDL_ttf.h>

class SDLFont
{
public:
	SDLFont();
	SDLFont(int x, int y, std::string str);
	~SDLFont();

	void pos(int x, int y) { m_x = x; m_y = y; }
	void font(std::string font_name, int font_size=DEF_FONT_SIZE);
	void text(std::string str);
	void color(SDL_Color val) { m_textColor = val; }

	void Render() const;
protected:
	static const int DEF_FONT_SIZE = 16;

	static boost::mutex m_ioMutex;

	int m_x, m_y;

	SDL_Surface * m_text;
	TTF_Font	* m_font;
	SDL_Color	m_textColor;
};