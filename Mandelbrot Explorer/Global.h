#pragma once

class Global
{
public:
	static const int SCREEN_WIDTH = 1080;
	static const int SCREEN_HEIGHT = 720;
	static const int SCREEN_BPP = 32;
	static const int FRAMES_PER_SECOND = 30;
	static SDL_Surface* screen;
	static SDL_Event e;

	static const int NUM_WORKER_THREADS = 4;

	static boost::barrier bar;
};