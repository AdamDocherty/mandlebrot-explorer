#include "stdafx.h"
#include "Application.h"

bool Application::s_quitting = false;
boost::mutex Application::s_inputMutex;

Application::Application()
{
	m_image = NULL;
}

Application::~Application()
{
}

void Application::Run()
{
	Init();

	while(!quitting())
	{
		Update();
		Render();

		// flips the buffers
		SDL_Flip(Global::screen);
		// clears the back buffer
		SDL_FillRect(Global::screen,NULL, 0x000000); 
	}
	CleanUp();
}

void Application::Init()
{
	m_mouseX = 0;
	m_mouseY = 0;

	// set font colour to white
	m_fontColour.r = 0; m_fontColour.g = 0; m_fontColour.b = 0;

	// initialise first image
	DrawImage(Rect(1, -1, -1.5, 0.5), 255);

	// initialise extra fonts
	for (int i = 0; i < NUM_EXTRA_FONT; ++i)
	{
		m_fonts[i] = NULL;
	}
}

void Application::Update()
{
	while( SDL_PollEvent( &Global::e ) )
	{
		switch (Global::e.type)
		{
		case SDL_QUIT:
			s_quitting = true;
			break;
		case SDL_MOUSEMOTION:
			// update mouse positions
			m_mouseX = Global::e.motion.x;
			m_mouseY = Global::e.motion.y;
		case SDL_KEYDOWN:
			// waits for the image to finish drawing before actions can be made
			if (m_image->imageDrawing()) break;
			switch( Global::e.key.keysym.sym )
			{
			// zooms in on the image
			case SDLK_c: 
				m_inputThread = boost::thread( boost::bind ( &Application::SetCoordinates, this ) );
				break;
			// draws the default image
			case SDLK_d: DrawImage(Rect(1, -1, -1.5, 0.5), 255); break;
			// changes the number of iterations
			case SDLK_i: 
				m_inputThread = boost::thread( boost::bind ( &Application::SetIterations, this ) );
				break;
			// Redraws the image, useful for checking times again
			case SDLK_r: RedrawImage(); break;
			// Keypad numbers, all pre-selected coordinates
			case SDLK_KP1: DrawImage(Rect(0.565078, 0.560781, -0.462979, -0.458746), 255); break;
			case SDLK_KP2: DrawImage(Rect(0.355806, 0.355755, -1.26169, -1.26163), 255); break;
			case SDLK_KP3: DrawImage(Rect(0.660245, 0.659231, -0.375016, -0.372708), 255); break;
			case SDLK_KP4: DrawImage(Rect(0.564434, 0.556348, 0.181445, 0.188584), 510); break;
			case SDLK_KP5: DrawImage(Rect(-0.310645, -0.321089, -1.20339, -1.19023), 255); break;
			case SDLK_KP6: DrawImage(Rect(2, -2, -3, 1), 255); break;
			case SDLK_KP7: DrawImage(Rect(0.959375, 0.59375, -0.284375, 0.003125), 255); break;
			case SDLK_KP8: DrawImage(Rect(0.0000704546, -0.000111705, 0.25008, 0.251191), 255); break;
			case SDLK_KP9: DrawImage(Rect(0.0000704546, -0.000111705, 0.25008, 0.251191), 510); break;
			default: ;
			}
		default: ;
		}
	}
}

void Application::Render()
{
	m_image->Render(X_OFFSET, Y_OFFSET);
	PrintMousePos();

	// prints controls
	SDLFont(20, 686, 
		"Controls: C - change coords, I - change iterations, R - redraw, D - default, NUMPAD - presets").Render();

	// prints extra fonts
	for (int i = 0; i < NUM_EXTRA_FONT; ++i)
	{
		if (NULL != m_fonts[i])
			m_fonts[i]->Render();
	}
}

void Application::CleanUp()
{
	delete m_image;
	for (int i = 0; i < NUM_EXTRA_FONT; ++i)
	{
		if(m_fonts[i] != NULL)
			delete m_fonts[i];
	}
}

void Application::SetCoordinates()
{
	boost::lock_guard<boost::mutex> locker(s_inputMutex);

	// creates text for instruction
	m_fonts[0] = new SDLFont(60 + MandelbrotImage::IMAGE_WIDTH, 380, "Click the top-left corner of");
	m_fonts[1] = new SDLFont(60 + MandelbrotImage::IMAGE_WIDTH, 400, "the area you want to zoom in on");

	bool valueEntered = false;
	bool firstValue = true;
	Rect newCoordinates;

	int mouseX, mouseY;

	while (!valueEntered)
	{
		while( SDL_PollEvent( &Global::e ) )
		{
			if( Global::e.type == SDL_MOUSEMOTION )
			{
				// update mouse positions
				mouseX = Global::e.motion.x;
				mouseY = Global::e.motion.y;
			}

			if( (Global::e.type == SDL_MOUSEBUTTONDOWN ) && ( Global::e.button.button == SDL_BUTTON_LEFT ) )
			{
				if((mouseX > X_OFFSET) && (mouseX < (X_OFFSET + MandelbrotImage::IMAGE_WIDTH)) && 
				   (mouseY > Y_OFFSET) && (mouseY < (Y_OFFSET + MandelbrotImage::IMAGE_HEIGHT)))
				{
					double x_coord = mouseX - X_OFFSET;
					double y_coord = mouseY - Y_OFFSET;

					m_image->GetCoordinateAt(x_coord, y_coord);

					// the first click will set the top left corner
					// the second click will set the bottom right corner
					if (firstValue)
					{
						newCoordinates.left = x_coord;
						newCoordinates.top = y_coord;
						firstValue = false;
						m_fonts[0]->text("Click the bottom-right corner of");
					} else {
						newCoordinates.right = x_coord;
						newCoordinates.bottom = y_coord;
						valueEntered = true;
					}
				}
			}

			// pressing Escape will cancel the operation
			if( ( Global::e.type == SDL_KEYDOWN ) && ( Global::e.key.keysym.sym == SDLK_ESCAPE ) )
			{
				newCoordinates = m_image->coord();
				valueEntered = true;
			}
		}
	}

	// delete the font objects used in the thread
	delete m_fonts[0];
	delete m_fonts[1];
	m_fonts[0] = NULL;
	m_fonts[1] = NULL;

	if (newCoordinates != m_image->coord())
		DrawImage(newCoordinates, m_image->maxIterations());

}

void Application::SetIterations()
{
	boost::lock_guard<boost::mutex> inputLocker(s_inputMutex);

	// creates an object for entering the text
	StringInput* input = new StringInput();
	input->pos(60 + MandelbrotImage::IMAGE_WIDTH + 10, 400);

	// creates the font objects so the text will be rendered
	m_fonts[0] = new SDLFont(60 + MandelbrotImage::IMAGE_WIDTH, 380, "Enter Iterations (1 - 4000)");
	m_fonts[1] = input;

	bool valueEntered = false;
	int newIterations = 0;

	while (!valueEntered)
	{
		while( SDL_PollEvent( &Global::e ) )
        {
			input->HandleInput();

			if( ( Global::e.type == SDL_KEYDOWN ) && ( Global::e.key.keysym.sym == SDLK_RETURN ) )
			{
				// if input is not a valid int, it will return 0
				newIterations = input->GetInt();
				// keeps the number in a range
				if (newIterations > 4000)
					newIterations = 4000;
				if (newIterations < 1)
					newIterations = 1;

				valueEntered = true;
			}

			// user can press escape to cancel the operation
			if( ( Global::e.type == SDL_KEYDOWN ) && ( Global::e.key.keysym.sym == SDLK_ESCAPE ) )
			{
				newIterations = m_image->maxIterations();
				valueEntered = true;
			}
		}
	}

	// deletes the font objects created in the thread
	delete m_fonts[0];
	delete m_fonts[1];
	m_fonts[0] = NULL;
	m_fonts[1] = NULL;

	// only redraws the image if the number of iterations has changed
	if (newIterations != m_image->maxIterations())
		DrawImage(m_image->coord(), newIterations);
}

void Application::DrawImage(const Rect& coordinates, int iterations)
{
	// deletes the current image if it exists
	if (NULL != m_image)
		delete m_image;
	// creates a new image
	m_image = new MandelbrotImage(coordinates, iterations);
	m_image->Compute();
}

void Application::RedrawImage()
{
	// Redraws the image with the same coordinates and iterations, useful to get another time
	if (NULL == m_image)
		return;
	DrawImage(m_image->coord(), m_image->maxIterations());
}

void Application::PrintMousePos() const
{
	// if the mouse is over the image
	if((m_mouseX > X_OFFSET) && (m_mouseX < (X_OFFSET + MandelbrotImage::IMAGE_WIDTH)) && 
	   (m_mouseY > Y_OFFSET) && (m_mouseY < (Y_OFFSET + MandelbrotImage::IMAGE_HEIGHT)))
	{
		double x_coord = m_mouseX - X_OFFSET;
		double y_coord = m_mouseY - Y_OFFSET;

		m_image->GetCoordinateAt(x_coord, y_coord);

		std::stringstream ss;

		ss  << "Mouse X = " << x_coord;
		SDLFont(60 + MandelbrotImage::IMAGE_WIDTH, 320, ss.str()).Render();
		ss.str(std::string()); ss.clear(); // clear stringstream
		ss  << "Mouse Y = " << y_coord;
		SDLFont(60 + MandelbrotImage::IMAGE_WIDTH, 340, ss.str()).Render();
	}
}