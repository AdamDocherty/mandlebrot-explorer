#include "stdafx.h"
#include "Global.h"

SDL_Surface* Global::screen = NULL;
SDL_Event Global::e;

boost::barrier Global::bar(NUM_WORKER_THREADS + 1);