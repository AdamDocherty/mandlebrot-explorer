// This file defines the entry point for the program

#include "stdafx.h"
#include "Application.h"

Uint32 color = 0;


void Init()
{
	SDL_Init(SDL_INIT_VIDEO);
	TTF_Init();
	Global::screen = SDL_SetVideoMode(Global::SCREEN_WIDTH,Global::SCREEN_HEIGHT,Global::SCREEN_BPP,SDL_HWSURFACE);
	SDL_WM_SetCaption("Mandelbrot Set",NULL);
}

int main(int argc, char* argv[])
{
	Init();

	Application app;

	app.Run();

	SDL_Quit();
	return 0;
}