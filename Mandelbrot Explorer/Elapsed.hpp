#pragma once
//! \file Elapsed.hpp
//! \author Jeff Benshetler
//! \date 2012-05-05
//! Licensed under \link http://www.boost.org/LICENSE_1_0.txt Boost Software License 1.0 \endlink
#include <boost/date_time.hpp>
// Purpose: track elapsed time from constructor or reset
class Elapsed {
	//! Construction time or time when last reset()
	boost::posix_time::ptime m_when;
public:
	//! Convenience function that returns the current high-resolution system time in UTC
	static boost::posix_time::ptime now() { return boost::posix_time::microsec_clock::universal_time(); }
	//! Begin tracking time when constructed
	Elapsed() : m_when( now() )
	{
	} // end constructor
	//! Reset the beginning of the elapsed time period to when reset() is called
	void reset() { m_when = now(); }
	//! Function call operator to retrieve the elapsed time
	boost::posix_time::time_duration operator()() const {
		return now() - m_when;
	} // end operator()
}; // end class Elapsed()