#pragma once

#include <boost/atomic/atomic.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <cmath>
#include <complex>
#include <string>
#include <sstream>

#include "Elapsed.hpp"
#include "Rect.h"
#include "SDLFont.h"
	
class MandelbrotImage
{
public:
	MandelbrotImage(const Rect& coordinates, int iterations);
	~MandelbrotImage();

	void Compute();
	void Render(int x, int y) const;

	void GetCoordinateAt(double& x, double& y);

	Rect coord() { return m_coord; }
	int  maxIterations() { return m_maxIterations; }
	bool imageDrawing() { return m_imageDrawing; }

	static const int IMAGE_HEIGHT = 640;
	static const int IMAGE_WIDTH = 640;
private:
	void Init();
	void InitPalette();

	void RecordTime();
	void ComputeSlice(int slice);
	void ComputeRow(int y);
	void SetPixel(int x,int y, Uint32 colour);
	
	void PrintDetails(int x, int y) const;
	void PrintTimes(int x, int y) const;

	Uint32 GetColour(int iterations);
	static SDL_Color HuetoRGB(float h);

	boost::thread_group m_tgroup;

	SDL_Surface* m_surface;
	Rect m_coord;
	int  m_maxIterations;
	bool m_imageDrawing;
	// atomic int used to store the number of pixels in the set
	boost::atomic<int> m_numInSet;

	// barrier used to record the time taken for the image to complete
	boost::barrier m_timeBarrier;
	boost::posix_time::time_duration m_time;
	boost::posix_time::time_duration m_timePerThread[Global::NUM_WORKER_THREADS];
};

