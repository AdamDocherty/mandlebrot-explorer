#include "StdAfx.h"
#include "StringInput.h"


StringInput::StringInput() :
	SDLFont()
{
	m_str = "";
	SDL_EnableUNICODE(SDL_ENABLE);
}

StringInput::~StringInput()
{
	SDL_EnableUNICODE(SDL_DISABLE);
}

void StringInput::HandleInput()
{
	if (Global::e.type == SDL_KEYDOWN)
	{
		std::string temp = m_str;

		if(m_str.length() <= MAX_LENGTH)
		{
			// only enable numbers
            if( ( Global::e.key.keysym.unicode >= (Uint16)'0' ) && ( Global::e.key.keysym.unicode <= (Uint16)'9' ) )
            {
                m_str += (char) Global::e.key.keysym.unicode;
            }
			else if ( Global::e.key.keysym.unicode == (Uint16)'.' )
			{
				m_str += (char) Global::e.key.keysym.unicode;
			}
		}

		if( ( Global::e.key.keysym.sym == SDLK_BACKSPACE ) && ( m_str.length() != 0 ) )
        {
            m_str.erase( m_str.length() - 1 );
        }

		if( m_str != temp )
        {
            text(m_str);
        }
	}
}

double StringInput::GetDouble() const
{
	double temp;
	return boost::lexical_cast<double>(m_str);
}

int StringInput::GetInt() const
{
	int temp;
	try {
		temp = boost::lexical_cast<int>(m_str);
	} catch ( boost::bad_lexical_cast )
	{
		return 0;
	}
	return temp;
}