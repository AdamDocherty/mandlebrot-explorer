#include "StdAfx.h"
#include "MandelbrotImage.h"

MandelbrotImage::MandelbrotImage(const Rect& coordinates, int iterations) :
	m_timeBarrier(Global::NUM_WORKER_THREADS + 1)
{
	m_coord = coordinates;
	m_maxIterations = iterations;
	m_numInSet = 0;
	m_imageDrawing = false;

	Init();
}

MandelbrotImage::~MandelbrotImage()
{
	// interupts all the threads when the image is deleted if they are still running
	m_tgroup.interrupt_all();
	SDL_FreeSurface(m_surface);
}

void MandelbrotImage::Init()
{
	m_surface = SDL_CreateRGBSurface(SDL_SWSURFACE, IMAGE_WIDTH, IMAGE_HEIGHT, 8,
		0, 0, 0, 0);

	InitPalette();
}

void MandelbrotImage::InitPalette()
{	
	SDL_Color colours[256];

	colours[0].r = 0;
	colours[0].g = 0;
	colours[0].b = 0;

	for(int i = 1; i < 256; ++i){
		colours[i] = HuetoRGB(static_cast<float>(i));
	}

	// set the palette to 8 BPP                                    v-- 256 colours
	SDL_SetPalette(m_surface, SDL_LOGPAL|SDL_PHYSPAL, colours, 0, 256);
}

// Function for converting the hue of a colour into an RGB value. Adapted from
// http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript
SDL_Color MandelbrotImage::HuetoRGB(float h)
{
	float r, g, b;
	h /= 255;

	int i = static_cast<int>(floor(h * 6));
	float f = h * 6 - i;
	float p = 0;
	float q = 1 - f;
	float t = 1 - (1 - f);

	switch(i % 6)
	{
	case 0: r = 1; g = t; b = p; break;
	case 1: r = q; g = 1; b = p; break;
	case 2: r = p; g = 1; b = t; break;
	case 3: r = p; g = q; b = 1; break;
	case 4: r = t; g = p; b = 1; break;
	case 5: r = 1; g = p; b = q; break;
	}

	SDL_Color colour;
	colour.r = static_cast<Uint8>(r * 255);
	colour.g = static_cast<Uint8>(g * 255);
	colour.b = static_cast<Uint8>(b * 255);
	return colour;
}

Uint32 MandelbrotImage::GetColour(int iterations)
{
	// returns black
	if (iterations == 0) return 0;

	// returns a number from 1 to 255
	return (iterations % 255) + 1;
}

void MandelbrotImage::SetPixel(int x,int y, Uint32 colour)
{
	// change the colour of a pixel on the image
	Uint8 * pixel = (Uint8*)m_surface->pixels;
	pixel += (y * m_surface->pitch) + (x * sizeof(Uint8));
	*pixel = colour;
}

void MandelbrotImage::Compute()
{
	m_imageDrawing = true;

	// create timing thread
	m_tgroup.create_thread( boost::bind( &MandelbrotImage::RecordTime, this ) );
	// create computation threads
	for (int i = 0; i < Global::NUM_WORKER_THREADS; ++i)
	{
		m_tgroup.create_thread( boost::bind( &MandelbrotImage::ComputeSlice, this, i ) );
	}
}

void MandelbrotImage::RecordTime()
{
	m_timeBarrier.wait();
	Elapsed etime;

	// wait for worker threads to finish
	m_timeBarrier.wait();
	// set total time
	m_time = etime();
	m_imageDrawing = false;
}

void MandelbrotImage::ComputeSlice(int slice)
{
	m_timeBarrier.wait();
	Elapsed etime;
	// slice determines which segment of the image is calculated
	// slice = 0 means the top segment, number of segments is equal to the
	// number of worker threads
	for (int y = slice*(IMAGE_HEIGHT/Global::NUM_WORKER_THREADS); 
		y < (1 + slice)*(IMAGE_HEIGHT/Global::NUM_WORKER_THREADS); 
		++y)
	{
		ComputeRow(y);
	}

	m_timePerThread[slice] = etime();
	m_timeBarrier.wait();
}

void MandelbrotImage::ComputeRow(int y)
{
	for (int x = 0; x < IMAGE_WIDTH; ++x)
	{
		// Work out the point in the complex plane that
		// corresponds to this pixel in the output image.
		std::complex<double> c(m_coord.left + (x * (m_coord.right  - m_coord.left) / IMAGE_WIDTH),
			                   m_coord.top  + (y * (m_coord.bottom - m_coord.top) / IMAGE_HEIGHT));

		// Start off z at (0, 0).
		std::complex<double> z(0.0, 0.0);

		// Iterate z = z^2 + c until z moves more than 2 units
		// away from (0, 0), or we've iterated too many times.
		int iterations = 0;
		while (abs(z) < 2.0 && iterations < m_maxIterations) {
			z = (z * z) + c;

			++iterations;
		}

		// provides a point at which the thread can be interrupted incase
		// the image is deleted before the calculations are finished
		boost::this_thread::interruption_point();

		if (iterations == m_maxIterations) {
			SetPixel(x, y, GetColour(0));
		} else {
			SetPixel(x, y, GetColour(iterations));
			m_numInSet += 1; // atomic add so it is threading safe
		}
	}
}

void MandelbrotImage::GetCoordinateAt(double& x, double& y)
{
	// converts a position from screen coordinates to image coordinates
	x = ((m_coord.right - m_coord.left) * (x/(double)IMAGE_WIDTH)) + m_coord.left;
	y = ((m_coord.bottom - m_coord.top) * (y/(double)IMAGE_HEIGHT)) + m_coord.top;
}

void MandelbrotImage::PrintDetails(int x, int y) const
{
	std::stringstream ss;

	ss  << "Top    = " << m_coord.top;
	SDLFont(x, y, ss.str()).Render();

	ss.str(std::string()); ss.clear(); // clear stringstream
	ss  << "Bottom = " << m_coord.bottom;
	SDLFont(x, y + 20, ss.str()).Render();

	ss.str(std::string()); ss.clear(); // clear stringstream
	ss  << "Left   = " << m_coord.left;
	SDLFont(x, y + 40, ss.str()).Render();

	ss.str(std::string()); ss.clear(); // clear stringstream
	ss  << "Right = " << m_coord.right;
	SDLFont(x, y + 60, ss.str()).Render();

	ss.str(std::string()); ss.clear(); // clear stringstream
	ss  << "Iterations = " << m_maxIterations;
	SDLFont(x, y + 80, ss.str()).Render();

	ss.str(std::string()); ss.clear(); // clear stringstream
	ss  << "% in set = " << ((100*(float) m_numInSet)/(float) (IMAGE_WIDTH*IMAGE_HEIGHT)) << "%";
	SDLFont(x, y + 100, ss.str()).Render();

	if(m_time.total_milliseconds() > 0)
	{
		PrintTimes(x, y + 140);
	}
}

void MandelbrotImage::PrintTimes(int x, int y) const
{
	std::stringstream ss;

	ss.str(std::string()); ss.clear(); // clear stringstream
	ss  << "Time(ms) = " << m_time.total_milliseconds() << "ms";
	SDLFont(x, y, ss.str()).Render();

	SDLFont(x, y + 20, "Spesific Thread Times (ms)").Render();
	for (int i = 0; i < Global::NUM_WORKER_THREADS; ++i)
	{
		ss.str(std::string()); ss.clear(); // clear stringstream
		ss  << "   " << i + 1 <<" = " << m_timePerThread[i].total_milliseconds() << "ms";
		SDLFont(x, y+(20*(i+2)), ss.str()).Render();
	}
}

void MandelbrotImage::Render(int x, int y) const
{
	SDL_Rect offset;
	offset.x = x;
	offset.y = y;
	SDL_BlitSurface( m_surface, NULL, Global::screen, &offset );

	PrintDetails(20 + x + IMAGE_WIDTH, y);
}