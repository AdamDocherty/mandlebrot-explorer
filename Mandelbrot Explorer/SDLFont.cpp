#include "StdAfx.h"
#include "SDLFont.h"

boost::mutex SDLFont::m_ioMutex;

SDLFont::SDLFont()
{
	m_x = 0;
	m_y = 0;
	font("default.ttf");
	m_textColor.r = 255;
	m_textColor.g = 255;
	m_textColor.b = 255;
	text("");
}

SDLFont::SDLFont(int x, int y, std::string str)
{
	m_x = x;
	m_y = y;
	font("default.ttf");
	m_textColor.r = 255;
	m_textColor.g = 255;
	m_textColor.b = 255;
	text(str);
}


SDLFont::~SDLFont()
{
	if (m_text != NULL) SDL_FreeSurface(m_text);
	TTF_CloseFont(m_font);
}

void SDLFont::font(std::string font_name, int font_size)
{
	m_font = TTF_OpenFont(font_name.c_str(), font_size);
	if (m_font == NULL)
	{
		MessageBoxA(NULL, "Font unable to load.", NULL, MB_ICONERROR);
	}
}

void SDLFont::text(std::string str)
{
	boost::lock_guard<boost::mutex> locker(m_ioMutex);
	m_text = TTF_RenderText_Solid(m_font, str.c_str(), m_textColor); 
}

void SDLFont::Render() const
{
	if (m_text == NULL) return;

	SDL_Rect offset;
	offset.x = m_x;
	offset.y = m_y;
	SDL_BlitSurface( m_text, NULL, Global::screen, &offset );
}