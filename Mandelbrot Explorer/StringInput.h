#pragma once

#include <boost\lexical_cast.hpp>
#include "SDLFont.h"

class StringInput : public SDLFont	
{
public:
	StringInput();
	~StringInput();

	void HandleInput();

	double GetDouble() const;
	int    GetInt() const;
private:
	static const int MAX_LENGTH = 32;

	std::string m_str;
};

