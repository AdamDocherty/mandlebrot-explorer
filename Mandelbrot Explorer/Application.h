#pragma once

#include <boost\thread\mutex.hpp>
#include "MandelbrotImage.h"
#include "Rect.h"
#include "StringInput.h"

class Application
{
public:
	Application();
	~Application();

	void Run();

	static bool quitting() { return s_quitting; }
	static void quitting(bool val) { s_quitting = val; }
private:
	void Init();
	void Update();
	void Render();
	void CleanUp();

	void DrawImage(const Rect& coordinates, int iterations);
	void RedrawImage();

	void SetCoordinates();
	void SetIterations();

	void PrintMousePos() const;

	static const int X_OFFSET = 40;
	static const int Y_OFFSET = 40;

	static bool s_quitting;
	static boost::mutex s_inputMutex;

	boost::thread m_inputThread;

	MandelbrotImage* m_image;

	int m_mouseX, m_mouseY;

	SDL_Color m_fontColour;
	SDL_Surface* m_textCurrentCoordinates;

	//Some strings to print instructions
	static const int NUM_EXTRA_FONT = 2;

	boost::mutex m_fontMutex;
	SDLFont* m_fonts[NUM_EXTRA_FONT];
};

