An Interactive Mandlebrot Set Explorer.

##Development
Uses SDL 1.2.13 for graphics.
Chose to use Boost 1.53.0 for threading instead of C++11 `<thread>` because MSVC10 did not support all the features I needed such as `<atomic>` and `<chrono>`.

##Features
* The User can zoom in as far as they'd like (eventually you will reach a point where the floating point numbers cannot be accurately split up giving a pixelated effect)
* The user can edit variables such as the number of iterations.
* The user can choose how many threads to use.
* The user can measure the time taken for the image to be generated.
* The user can change the method used to divide up the image for threads.

##Screenshots
![Mandlebrot Screenshot](http://i.imgur.com/mvYkDLu.png)

Chunk mode
![Chunk mode](http://i.imgur.com/8CqFw6w.png)

Interlaced mode
![Interlaced mode](http://i.imgur.com/8Ozwkl0.png)

##Feedback

>Your prelimary mark for this assessment is 20 (using the Abertay
0-20 mark scheme; see the assessment specification for details of how
this was calculated from the individual marks below).

># ADAM DOCHERTY

>## Quality (A)

>Nice tidy easy-to-follow code, making good use of Boost and SDL.

>(Very picky: if you're writing portable code, be careful about filename
case -- you can't say #include "foo.h" if it's actually called "Foo.h".)

>## Techniques (A)

>Your code demonstrates all the required programming techniques.

>## Evaluation (A)

>Good presentation of results and reasonable explanation.

>## Presentation (A)

>Excellent presentation and discussion. A bit more on the class structure
would have been useful?


I also did a presentation about this program, which can be found [here](https://www.dropbox.com/s/4ea4jf7xk3edy8t/Presentation.pdf).